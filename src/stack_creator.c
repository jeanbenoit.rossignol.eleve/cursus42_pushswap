/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_creator.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/10 08:43:48 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/25 18:12:08 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	create_stack(t_object *obj, char **argv, int argc)
{
	t_stack	*stack;
	int		i;
	long	val;

	obj->stack_a = NULL;
	stack = NULL;
	i = 0;
	while (argv[i])
	{
		val = check_val(argv[i], obj, argv, argc);
		ft_llstadd_back(&stack, ft_llstnew(val));
		i++;
	}
	obj->stack_a = stack;
	check_dup(obj, argv, argc);
	if (check_sorted(stack))
	{
		if (argc == 2)
			free_split(argv);
		free_struct(obj);
		exit(0);
	}
}
