/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/11 06:21:29 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/25 15:36:03 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	sort_3_numbers(t_object *obj, t_stack *last_node)
{
	if (obj->stack_a->index > obj->stack_a->next->index)
	{
		if (obj->stack_a->next->index == obj->first_median
			&& obj->stack_a->index < last_node->index)
			rev_rotate_a(obj);
		else if (obj->stack_a->next->index == obj->first_median
			&& obj->stack_a->index > last_node->index)
			rotate_a(obj);
		else if (last_node->index == obj->first_median)
			rotate_a(obj);
		else
			swap_a(obj);
	}
	else if (obj->stack_a->index < obj->stack_a->next->index)
	{
		if (last_node->index == obj->first_median
			& last_node->index > obj->stack_a->index)
			rev_rotate_a(obj);
		else if (last_node->index == obj->first_median)
			rotate_a(obj);
		else
			rev_rotate_a(obj);
	}
}

void	push_min(t_object *obj)
{
	int	min;
	int	rot;

	min = get_stack_min(obj->stack_b);
	rot = get_rotate(obj->stack_b, min);
	if (rot > (ft_llst_length(obj->stack_b) / 2))
	{
		while (obj->stack_b->index != min)
			rev_rotate_b(obj);
	}
	else
	{
		while (obj->stack_b->index != min)
			rotate_b(obj);
	}
	push_a(obj);
	rotate_a(obj);
}

void	put_min(t_object *obj)
{
	t_stack	*last_node;

	last_node = ft_llst_last(obj->stack_a);
	if (obj->stack_b->index == obj->current_index)
	{
		push_a(obj);
		rotate_a(obj);
		obj->current_index += 1;
	}
}

int	get_rotate(t_stack *stack, int nb)
{
	int		i;
	t_stack	*node;

	i = 0;
	node = stack;
	while (node->index != nb)
	{
		node = node->next;
		i++;
	}
	return (i);
}

int	*sort_table(int	*table, int length)
{
	int	i;
	int	j;
	int	temp;

	i = 0;
	j = 1;
	while (j < length)
	{
		if (table[i] > table[j])
		{
			temp = table[j];
			table[j] = table[i];
			table[i] = temp;
			i = 0;
			j = 1;
		}
		else
		{
			i++;
			j++;
		}
	}
	return (table);
}
