/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_move_push.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 11:20:08 by jrossign          #+#    #+#             */
/*   Updated: 2022/03/28 08:49:51 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	push_b(t_object *obj)
{
	t_stack	*temp_node;

	temp_node = obj->stack_a;
	if (ft_llst_length(obj->stack_a) == 1)
		obj->stack_a = NULL;
	else
	{
		obj->stack_a = obj->stack_a->next;
		obj->stack_a->prev = NULL;
	}
	if (ft_llst_length(obj->stack_b) == 0)
	{
		temp_node->next = NULL;
		temp_node->prev = NULL;
		obj->stack_b = temp_node;
	}
	else
	{
		obj->stack_b->prev = temp_node;
		temp_node->next = obj->stack_b;
		obj->stack_b = temp_node;
	}
	printf("pb\n");
}

void	push_a(t_object *obj)
{
	t_stack	*temp_node;

	temp_node = obj->stack_b;
	if (ft_llst_length(obj->stack_b) == 1)
		obj->stack_b = NULL;
	else
	{
		obj->stack_b = obj->stack_b->next;
		obj->stack_b->prev = NULL;
	}
	if (ft_llst_length(obj->stack_a) == 0)
	{
		temp_node->next = NULL;
		temp_node->prev = NULL;
		obj->stack_a = temp_node;
	}
	else
	{
		obj->stack_a->prev = temp_node;
		temp_node->next = obj->stack_a;
		obj->stack_a = temp_node;
	}
	printf("pa\n");
}
