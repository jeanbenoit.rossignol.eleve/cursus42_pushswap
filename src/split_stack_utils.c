/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_stack_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/21 10:17:00 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/21 11:24:46 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	split_stack_opti(t_object *obj, int median)
{
	int	half_median;

	half_median = median / 2;
	while (!check_stack_content_a(obj->stack_a, median))
	{
		if (obj->stack_a->index < obj->first_median)
		{
			if (obj->stack_a->index < half_median)
			{
				push_b(obj);
				rotate_b(obj);
			}
			else
				push_b(obj);
		}
		else
			rotate_a(obj);
	}
}

void	split_stack_opti_next(t_object *obj, int median)
{
	while (obj->stack_a->index != get_stack_min(obj->stack_a))
	{
		if (obj->stack_a->index > obj->first_median)
		{
			if (obj->stack_a->index < (median + (median / 2)))
			{
				push_b(obj);
				rotate_b(obj);
			}
			else
				push_b(obj);
		}
		else
			rotate_a(obj);
	}
}

void	re_split_stack(t_object *obj)
{
	if (obj->first_median < obj->second_median)
	{
		while (obj->stack_a->index > obj->last_index)
			push_b(obj);
		if (ft_llst_length(obj->stack_b) == 0)
		{
			while (obj->stack_a->index > obj->last_index)
				push_b(obj);
		}
	}
	else
	{
		while (obj->stack_a->index <= obj->second_index)
			push_b(obj);
		if (ft_llst_length(obj->stack_b) == 0)
		{
			while (obj->stack_a->index < obj->last_index)
				push_b(obj);
		}
	}
}

void	split_stack(t_object *obj, int nb)
{
	while (!check_stack_content_a(obj->stack_a, nb))
	{
		if (obj->stack_a->index < obj->first_median)
			push_b(obj);
		else
			rotate_a(obj);
	}
}

void	split_stack_b(t_object *obj, int nb)
{
	int	i;

	i = 0;
	while (!check_stack_content_b(obj->stack_b, nb))
	{
		put_min(obj);
		if (obj->stack_b->index > obj->second_median)
			push_a(obj);
		else
			rotate_b(obj);
	}
}
