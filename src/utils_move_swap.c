/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_move_swap.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/21 14:07:42 by jrossign          #+#    #+#             */
/*   Updated: 2022/03/28 07:22:04 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	swap_a(t_object *obj)
{
	int	temp_val;

	if (ft_llst_length(obj->stack_a) < 2)
		return ;
	temp_val = obj->stack_a->val;
	obj->stack_a->val = obj->stack_a->next->val;
	obj->stack_a->next->val = temp_val;
	printf("sa\n");
}

void	swap_b(t_object *obj)
{
	int	temp_val;

	if (ft_llst_length(obj->stack_b) < 2)
		return ;
	temp_val = obj->stack_b->val;
	obj->stack_b->val = obj->stack_b->next->val;
	obj->stack_b->next->val = temp_val;
	printf("sb\n");
}

void	swap_s(t_object *obj)
{
	int	temp_val;

	if (ft_llst_length(obj->stack_a) > 2)
	{
		temp_val = obj->stack_a->val;
		obj->stack_a->val = obj->stack_a->next->val;
		obj->stack_a->next->val = temp_val;
	}
	if (ft_llst_length(obj->stack_b) > 2)
	{
		temp_val = obj->stack_b->val;
		obj->stack_b->val = obj->stack_b->next->val;
		obj->stack_b->next->val = temp_val;
	}
	printf("ss\n");
}
