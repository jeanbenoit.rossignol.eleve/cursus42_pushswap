/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 16:45:08 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/25 18:53:37 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

long	check_val(char *str, t_object *obj, char **argv, int argc)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '-' && i == 0)
			i++;
		if (!ft_isdigit(str[i]))
		{
			if (argc == 2)
				free_split(argv);
			free_struct(obj);
			print_error("Error\n");
		}
		i++;
	}
	return (check_min_max(str, obj, argv, argc));
}

long	check_min_max(char *str, t_object *obj, char **argv, int argc)
{
	long	val;

	val = ft_atoi(str);
	if (val < INT_MIN || val > INT_MAX)
	{
		if (argc == 2)
			free_split(argv);
		free_struct(obj);
		print_error("Error\n");
	}
	return (val);
}

void	check_dup(t_object *obj, char **argv, int argc)
{
	int		stack_length;
	int		i;
	int		*table;
	int		result;
	t_stack	*stack;

	stack = obj->stack_a;
	stack_length = ft_llst_length(stack);
	i = 0;
	table = malloc(sizeof(int) * stack_length);
	while (stack != NULL)
	{
		table[i] = stack->val;
		stack = stack->next;
		i++;
	}
	result = utils_check_dup(table, stack_length);
	free(table);
	if (result == 0)
	{
		if (argc == 2)
			free_split(argv);
		free_struct(obj);
		print_error("Error\n");
	}
}
