/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_move_revrotate.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 14:20:57 by jrossign          #+#    #+#             */
/*   Updated: 2022/03/28 06:05:11 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	rev_rotate_a(t_object *obj)
{
	t_stack	*last_node;

	if (ft_llst_length(obj->stack_a) < 2)
		return ;
	last_node = ft_llst_last(obj->stack_a);
	last_node->prev->next = NULL;
	obj->stack_a->prev = last_node;
	last_node->next = obj->stack_a;
	obj->stack_a = last_node;
	printf("rra\n");
}

void	rev_rotate_b(t_object *obj)
{
	t_stack	*last_node;

	if (ft_llst_length(obj->stack_b) < 2)
		return ;
	last_node = ft_llst_last(obj->stack_b);
	last_node->prev->next = NULL;
	obj->stack_b->prev = last_node;
	last_node->next = obj->stack_b;
	obj->stack_b = last_node;
	printf("rrb\n");
}

void	rev_rotate_r(t_object *obj)
{
	t_stack	*last_node_a;
	t_stack	*last_node_b;

	last_node_a = ft_llst_last(obj->stack_a);
	last_node_b = ft_llst_last(obj->stack_b);
	if (ft_llst_length(obj->stack_a) > 1)
	{
		last_node_a->prev->next = NULL;
		obj->stack_a->prev = last_node_a;
		last_node_a->next = obj->stack_a;
		obj->stack_a = last_node_a;
	}
	if (ft_llst_length(obj->stack_b) > 1)
	{
		last_node_b->prev->next = NULL;
		obj->stack_b->prev = last_node_b;
		last_node_b->next = obj->stack_b;
		obj->stack_b = last_node_b;
	}
	printf("rrr\n");
}
