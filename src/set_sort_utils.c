/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_sort_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/21 10:30:00 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/21 11:26:12 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	set_sort(t_object *obj)
{
	split_stack_opti(obj, obj->first_median);
	obj->last_index = obj->stack_a->index;
	obj->second_median = get_median(obj->stack_b);
	split_stack_b(obj, obj->second_median);
	obj->second_index = get_stack_max(obj->stack_b);
}

void	set_next_sort(t_object *obj)
{
	split_stack_opti_next(obj, obj->first_median);
	obj->last_index = 0;
	obj->second_median = get_median(obj->stack_b);
	split_stack_b(obj, obj->second_median);
	obj->second_index = get_stack_max(obj->stack_b);
}
