/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/08 12:27:53 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/26 12:05:01 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

int	main(int argc, char **argv)
{
	t_object	*obj;

	if (argc < 2)
		exit(0);
	obj = malloc(sizeof(t_object));
	obj->stack_b = NULL;
	argv++;
	if (argc == 2)
		argv = ft_split(argv[0], ' ');
	if (argv[0] != NULL)
	{
		create_stack(obj, argv, argc);
		set_index(obj);
		set_obj(obj);
		if (ft_llst_length(obj->stack_a) <= 5)
			sort_3(obj);
		else
		{
			set_sort(obj);
			sort_more(obj);
		}
	}
	if (argc == 2)
		free_split(argv);
	free_struct(obj);
}
