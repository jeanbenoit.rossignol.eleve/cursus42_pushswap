/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/16 09:23:54 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/21 09:44:00 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	print_stack_a(t_object *obj)
{
	int		i;
	t_stack	*stack;

	i = 0;
	stack = obj->stack_a;
	if (!stack)
		printf("The stack_a is empty\n");
	else
	{
		while (stack)
		{
			printf("Stack_a content %i: %i\n", i, stack->val);
			i++;
			stack = stack->next;
		}
	}
}

void	print_stack_b(t_object *obj)
{
	int		i;
	t_stack	*stack;

	i = 0;
	stack = obj->stack_b;
	if (!stack)
		printf("The stack_b is empty\n");
	else
	{
		while (stack)
		{
			printf("Stack_b content %i: %i\n", i, stack->val);
			i++;
			stack = stack->next;
		}
	}
}
