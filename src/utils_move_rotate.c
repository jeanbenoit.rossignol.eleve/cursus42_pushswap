/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_move_rotate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/23 15:26:04 by jrossign          #+#    #+#             */
/*   Updated: 2022/03/28 06:05:38 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	rotate_a(t_object *obj)
{
	t_stack	*last_node;
	t_stack	*temp_node;

	if (ft_llst_length(obj->stack_a) < 2)
		return ;
	last_node = ft_llst_last(obj->stack_a);
	temp_node = obj->stack_a;
	obj->stack_a = obj->stack_a->next;
	last_node->next = temp_node;
	temp_node->prev = last_node;
	temp_node->next = NULL;
	printf("ra\n");
}

void	rotate_b(t_object *obj)
{
	t_stack	*last_node;
	t_stack	*temp_node;

	if (ft_llst_length(obj->stack_b) < 2)
		return ;
	last_node = ft_llst_last(obj->stack_b);
	temp_node = obj->stack_b;
	obj->stack_b = obj->stack_b->next;
	last_node->next = temp_node;
	temp_node->prev = last_node;
	temp_node->next = NULL;
	printf("rb\n");
}

void	rotate_r(t_object *obj)
{
	t_stack	*last_node;
	t_stack	*temp_node;

	last_node = ft_llst_last(obj->stack_a);
	temp_node = obj->stack_a;
	if (ft_llst_length(obj->stack_b) > 1)
	{
		obj->stack_a = obj->stack_a->next;
		last_node->next = temp_node;
		temp_node->prev = last_node;
		temp_node->next = NULL;
	}
	last_node = ft_llst_last(obj->stack_b);
	temp_node = obj->stack_b;
	if (ft_llst_length(obj->stack_b) > 1)
	{
		obj->stack_b = obj->stack_b->next;
		last_node->next = temp_node;
		temp_node->prev = last_node;
		temp_node->next = NULL;
	}
	printf("rr\n");
}
