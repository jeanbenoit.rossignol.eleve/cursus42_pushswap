/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/21 11:13:03 by jrossign          #+#    #+#             */
/*   Updated: 2022/03/28 09:59:56 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

t_stack	*ft_llstnew(int val)
{
	t_stack	*node;

	node = malloc(sizeof(t_stack));
	if (!node)
		return (NULL);
	node->val = val;
	node->next = NULL;
	node->prev = NULL;
	return (node);
}

t_stack	*ft_llst_last(t_stack *stack)
{
	if (!stack)
		return (NULL);
	while (stack->next != NULL)
		stack = stack->next;
	return (stack);
}

t_stack	*ft_llst_last_node(t_stack *stack)
{
	t_stack	*temp_node;

	temp_node = stack;
	while (temp_node->next != NULL)
		temp_node = temp_node->next;
	return (temp_node);
}

void	ft_llstadd_back(t_stack **stack, t_stack *new_node)
{
	t_stack	*last_node;

	if (!*stack)
		*stack = new_node;
	else
	{
		last_node = ft_llst_last(*stack);
		last_node->next = new_node;
		new_node->prev = last_node;
	}
}

int	ft_llst_length(t_stack *stack)
{
	int	i;

	i = 0;
	while (stack != NULL)
	{
		stack = stack->next;
		i++;
	}
	return (i);
}
