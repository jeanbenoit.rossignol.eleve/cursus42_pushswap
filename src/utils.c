/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/10 11:14:44 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/25 18:10:19 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

int	utils_check_dup(int *table, int length)
{
	int	i;
	int	j;

	i = 0;
	j = length - 1;
	while (i < length)
	{
		while (j > 0)
		{
			if (table[i] == table[j] && i != j)
				return (0);
			j--;
		}
		j = length - 1;
		i++;
	}
	return (1);
}

void	free_struct(t_object *obj)
{
	t_stack	*node;

	free(obj->stack_b);
	while (obj->stack_a != NULL)
	{
		node = obj->stack_a;
		obj->stack_a = obj->stack_a->next;
		free(node);
	}
	free(obj);
}

void	free_split(char **table)
{
	int	i;

	i = 0;
	while (table[i])
	{
		free(table[i]);
		i++;
	}
	free(table);
}
