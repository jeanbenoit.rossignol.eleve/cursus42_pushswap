/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_stack_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/28 09:41:18 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/20 14:51:58 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

int	check_sorted(t_stack *stack)
{
	t_stack	*node;

	node = stack;
	while (node->next != NULL)
	{
		if (node->val > node->next->val)
			return (0);
		node = node->next;
	}
	return (1);
}

int	check_stack_content_a(t_stack *stack, int number)
{
	t_stack	*node;

	node = stack;
	while (node != NULL)
	{
		if (node->index < number)
			return (0);
		node = node->next;
	}
	return (1);
}

int	check_stack_content_b(t_stack *stack, int number)
{
	t_stack	*node;

	node = stack;
	while (node != NULL)
	{
		if (node->index > number)
			return (0);
		node = node->next;
	}
	return (1);
}

int	get_stack_max(t_stack *stack)
{
	t_stack	*node;
	int		max;

	node = stack;
	max = stack->index;
	while (node != NULL)
	{
		if (node->index > max)
			max = node->index;
		node = node->next;
	}
	return (max);
}

int	get_stack_min(t_stack *stack)
{
	t_stack	*node;
	int		min;

	node = stack;
	min = stack->index;
	while (node != NULL)
	{
		if (node->index < min)
			min = node->index;
		node = node->next;
	}
	return (min);
}
