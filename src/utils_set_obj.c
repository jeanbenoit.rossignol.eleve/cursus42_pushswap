/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_set_obj.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/31 09:42:49 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/25 15:37:23 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

int	get_median(t_stack *stack)
{
	t_stack	*node;
	int		max;
	int		min;
	int		i;
	int		length;

	node = stack;
	max = get_stack_max(stack);
	min = get_stack_min(stack);
	i = 0;
	length = (ft_llst_length(stack) / 2);
	while (i < length)
	{
		while (node != NULL)
		{
			if (node->index > min && node->index < max)
				min = node->index;
			node = node->next;
		}
		max = min;
		min = get_stack_min(stack);
		node = stack;
		i++;
	}
	return (max);
}

void	set_obj(t_object *obj)
{
	obj->length = ft_llst_length(obj->stack_a);
	obj->first_median = get_median(obj->stack_a);
	obj->current_index = 0;
}

void	set_index(t_object *obj)
{
	int		*table;
	int		i;
	t_stack	*node;

	node = obj->stack_a;
	table = (int *) malloc(sizeof(int) * (ft_llst_length(obj->stack_a) + 1));
	i = 0;
	while (node != NULL)
	{
		table[i] = node->val;
		node = node->next;
		i++;
	}
	sort_table(table, ft_llst_length(obj->stack_a));
	node = obj->stack_a;
	while (node != NULL)
	{
		get_index(node, ft_llst_length(obj->stack_a), table);
		node = node->next;
	}
	free(table);
}

void	get_index(t_stack *node, int length, int *table)
{
	int	i;

	i = 0;
	while (i < length)
	{
		if (node->val == table[i])
			node->index = i;
		i++;
	}
}
