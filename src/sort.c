/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/28 08:24:23 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/21 11:11:37 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/push_swap.h"

void	sort_3(t_object *obj)
{
	t_stack	*last_node;

	if (ft_llst_length(obj->stack_a) > 3)
	{
		split_stack(obj, obj->first_median);
		obj->first_median = get_median(obj->stack_a);
	}
	while (!check_sorted(obj->stack_a))
	{
		last_node = ft_llst_last(obj->stack_a);
		sort_3_numbers(obj, last_node);
	}
	merge_list(obj);
}

void	sort_more(t_object *obj)
{
	while (ft_llst_length(obj->stack_b) > 25)
	{
		obj->second_median = get_median(obj->stack_b);
		split_stack_b(obj, obj->second_median);
	}
	while (obj->stack_b != NULL)
		push_min(obj);
	re_split_stack(obj);
	if (ft_llst_length(obj->stack_b) > 0)
		obj->second_median = get_median(obj->stack_b);
	if (obj->first_median - obj->second_median > 1
		&& ft_llst_length(obj->stack_b) > 0)
		sort_more(obj);
	else
	{
		set_next_sort(obj);
		sort_next(obj);
	}
}

void	sort_next(t_object *obj)
{
	while (ft_llst_length(obj->stack_b) > 25)
	{
		obj->second_median = get_median(obj->stack_b);
		split_stack_b(obj, obj->second_median);
	}
	while (obj->stack_b != NULL)
		push_min(obj);
	re_split_stack(obj);
	if (ft_llst_length(obj->stack_b) > 0)
		obj->second_median = get_median(obj->stack_b);
	if (ft_llst_length(obj->stack_b) > 0)
		sort_next(obj);
}

void	merge_list(t_object *obj)
{
	while (obj->stack_b != NULL)
	{
		if (obj->stack_a->val > obj->stack_b->val)
			push_a(obj);
		else if (obj->stack_a->val < obj->stack_b->val)
		{
			rotate_a(obj);
			push_a(obj);
			rev_rotate_a(obj);
		}
		else
			push_a(obj);
	}
}
