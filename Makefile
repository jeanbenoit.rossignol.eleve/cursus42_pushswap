# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/03/03 15:42:13 by jrossign          #+#    #+#              #
#    Updated: 2022/04/21 11:41:51 by jrossign         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#--files--#
C_FILES		=	error.c print_stack.c push_swap.c set_sort_utils.c sort.c		\
				split_stack_utils.c stack_creator.c utils.c utils_move_push.c	\
				utils_move_revrotate.c utils_move_rotate.c utils_move_swap.c	\
				utils_set_obj.c utils_sort.c utils_stack.c utils_stack_2.c		\
				check_args.c													\

#--C FILES TO O FILES--#
O_FILES		= $(C_FILES:.c=.o)

#--NAME--#
NAME		= push_swap

#--COMMANDS--#
GCC			= gcc
RM_DIR		= rm -fr
RM_FILES	= rm -f
MAKE_C		= make -C
MKDIR		= mkdir
NORMINETTE	= norminette
GIT_INIT	= git submodule init
GIT_UPDATE	= git submodule update

#--FLAGS--#
ERROR_FLAGS	= -Wall -Werror -Wextra
LIBFT_FLAG	= -L./libft -lft
P_FLAG		= -p
O_FLAG		= -o
C_FLAG		= -c

#--PATH--#
OBJ_DIR		= obj/
SRC_DIR		= src/
INCLUDE_DIR	= include/
LIBFT_DIR	= libft/

#--VPATH--#
VPATH		= $(SRC_DIR)

#--PREFIX--#
PRE_SRC		= $(addprefix $(SRC_DIR), $(C_FILES))
PRE_OBJ		= $(addprefix $(OBJ_DIR), $(O_FILES))

#--ACTIONS--#
$(OBJ_DIR)%.o:		%.c
				@$(MKDIR) $(P_FLAG) $(OBJ_DIR)
				@$(GCC) $(ERROR_FLAGS) $(O_FLAG) $@ $(C_FLAG) $<

$(NAME):			$(PRE_OBJ)
				@$(GIT_INIT)
				@$(GIT_UPDATE)
				@echo "Compiling push_swap..."
				@$(MAKE_C) $(LIBFT_DIR)
				@$(GCC) $(CFLAGS) $(PRE_OBJ) $(O_FLAG) $(NAME) $(LIBFT_FLAG)
				@echo "Compiling push_swap done."

all:				$(NAME)

clean: 
				@echo "Removing push_swap object files..."
				@$(MAKE_C) $(LIBFT_DIR) clean
				@$(RM_FILES) $(PRE_OBJ)
				@$(RM_DIR) $(OBJ_DIR)
				@echo "Removing push_swap object files done."

fclean:				clean
				@echo "Removing push_swap program..."
				@$(MAKE_C) $(LIBFT_DIR) fclean
				@$(RM_FILES) $(NAME)
				@echo "Removing push_swap program done."

re:					fclean all

norminette:
				@$(GIT_INIT)
				@$(GIT_UPDATE)
				@$(NORMINETTE) $(SRC_DIR) $(INCLUDE_DIR) $(LIBFT_DIR)

.PHONY: all clean fclean re norminette
