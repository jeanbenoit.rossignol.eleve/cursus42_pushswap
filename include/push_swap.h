/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/03/02 08:29:41 by jrossign          #+#    #+#             */
/*   Updated: 2022/04/25 17:58:48 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <unistd.h>
# include <stdlib.h>
# include <limits.h>
# include <stdio.h>
# include "../libft/include/libft.h"

typedef struct s_stack
{
	int				val;
	int				index;
	struct s_stack	*next;
	struct s_stack	*prev;
}					t_stack;

typedef struct s_object
{
	t_stack	*stack_a;
	t_stack	*stack_b;
	int		first_median;
	int		second_median;
	int		small_split;
	int		length;
	int		last_index;
	int		second_index;
	int		current_index;
}				t_object;

//check_args.c
long		check_val(char *str, t_object *obj, char **argv, int argc);
long		check_min_max(char *str, t_object *obj, char **argv, int argc);
void		check_dup(t_object *obj, char **argv, int argc);

//error.c
void		print_error(char *error_message);

//print_stack.c
void		print_stack_a(t_object *obj);
void		print_stack_b(t_object *obj);

//push_swap.c
int			main(int argc, char *argv[]);

//set_sort_utils.c
void		set_sort(t_object *obj);
void		set_next_sort(t_object *obj);

//sort.c
void		sort_3(t_object *obj);
void		sort_more(t_object *obj);
void		sort_next(t_object *obj);
void		merge_list(t_object *obj);

//split_stack_utils.c
void		split_stack_opti(t_object *obj, int median);
void		split_stack_opti_next(t_object *obj, int median);
void		re_split_stack(t_object *obj);
void		split_stack(t_object *obj, int nb);
void		split_stack_b(t_object *obj, int nb);

//stack_creator.c
void		create_stack(t_object *obj, char **argv, int argc);

//utils.c
int			utils_check_dup(int *table, int length);
void		free_struct(t_object *obj);
void		free_split(char **table);

//utils_move_push.c
void		push_b(t_object *obj);
void		push_a(t_object *obj);

//utils_move_revrotate.c
void		rev_rotate_a(t_object *obj);
void		rev_rotate_b(t_object *obj);
void		rev_rotate_r(t_object *obj);

//utils_move_rotate.c
void		rotate_a(t_object *obj);
void		rotate_b(t_object *obj);
void		rotate_r(t_object *obj);

//utils_move_swap.c
void		swap_a(t_object *obj);
void		swap_b(t_object *obj);
void		swap_s(t_object *obj);

//utils_set_obj.c
int			get_median(t_stack *stack);
void		set_obj(t_object *obj);
void		set_index(t_object *obj);
void		get_index(t_stack *node, int length, int *table);

//utils_sort.c
void		sort_3_numbers(t_object *obj, t_stack *last_node);
void		push_min(t_object *obj);
void		put_min(t_object *obj);
int			get_rotate(t_stack *stack, int nb);
int			*sort_table(int	*table, int length);

//utils_stack.c
t_stack		*ft_llstnew(int val);
t_stack		*ft_llst_last(t_stack *stack);
t_stack		*ft_llst_last_node(t_stack *stack);
void		ft_llstadd_back(t_stack **stack, t_stack *new_node);
int			ft_llst_length(t_stack *stack);

//utils_stack_2.c
int			check_sorted(t_stack *stack);
int			check_stack_content_a(t_stack *stack, int number);
int			check_stack_content_b(t_stack *stack, int number);
int			get_stack_max(t_stack *stack);
int			get_stack_min(t_stack *stack);

#endif
